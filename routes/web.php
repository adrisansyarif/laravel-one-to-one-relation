<?php

use App\Models\Address;
use Illuminate\Support\Facades\Route;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/insert', function() {

    $user = User::findOrFail(2);
//     foreach($user->addresses as $address) {

//         return $address->name;
//     }

    $address = new Address(['name'=>'123 Katapan']);
    $user->addresses()->save($address);


});

Route::get('/update', function() {

    $address = Address::whereUserId(1)->first();

    $address->name = "223 Jakarta";

    $address->save();


});

Route::get('/read', function() {
    $user = User::findOrFail(1);

    echo $user->addresses->name;
});

Route::get('/delete', function() {
    $user = User::findOrFail(2);
    $user->addresses()->delete();
    return "done";
});
